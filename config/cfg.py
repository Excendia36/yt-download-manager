import os
import io
import yaml


def create_appdata_directory():
    path = os.path.join(os.getenv('APPDATA'), ".yt_downloader")
    if os.path.exists(path):
        print("[Config] Found appdata directory " + path)
    else:
        print("[Config] Creating appdata directory " + path)
        os.mkdir(path)
    return path


path_to_file = os.path.join(create_appdata_directory(), "config.yml")


def write_config(data):
    with io.open(path_to_file, 'w', encoding='utf8') as outfile:
        yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)


def read_config():
    # Read YAML file
    with open(path_to_file, 'r') as stream:
        data_loaded = yaml.safe_load(stream)
    if data_loaded is not None:
        return data_loaded
    return ""


def create_config():
    if os.path.isfile(path_to_file):
        print("[Config] Config Datei erkannt.")
        return False
    else:
        print("[Config] Erstelle config für Erstinitialisierung.")
        open(path_to_file, "w+")
        return True
