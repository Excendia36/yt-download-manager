import requests


def validate_url(url):
    r = requests.get(url)
    url_bool = "Video unavailable" in r.text
    if url_bool:
        print("[URL-Validator] Die URL '" + url + "' konnte durch einen Request nicht validiert werden.")
        return False
    print("[URL-Validator] Die URL '" + url + "' wurde durch einen Request validiert.")
    return True
