from unittest import TestCase


class Test(TestCase):
    def test_validate_url(self):
        self.assertTrue("https://www.youtube.com/watch?v=pyo2DMJqX4s")

    def test_validate_url1(self):
        self.assertTrue("https://www.youtube.com/watch?v=pyo2DMasfgds")

    def test_validate_url2(self):
        self.assertTrue("https://www.youtube.com/watch?v=dsg13rSg")

    def test_validate_url3(self):
        self.assertTrue("https://www.youtube.com/watch?v=pyo2gffe2")

    def test_validate_wring_url(self):
        self.assertTrue("https://www.gogleee.com")
