import traceback

import PySimpleGUI as sg

from download import download


def open_main_frame():
    try:
        sg.set_global_icon("youtube_ico.ico")
        layout = [[sg.Text(text="Hier den YouTube Link eingeben:")],
                  [sg.Input()],
                  [sg.Radio('mp4', "RADIO1", default=False)],
                  [sg.Radio('mp3', "RADIO1", default=True)],
                  [sg.Button('Ok', button_color="green"), sg.Exit("Beenden", button_color="red")]]

        window = sg.Window('YouTube Downloader', layout)

        while True:
            event, values = window.read()

            if values is None or values[0] is None or values[0] == "":
                print("[Main-Frame] URL war leer, öffne erneut.")
            else:
                print("[Main-Frame] URL '" + values[0] + "'")
                if values[1]:
                    download.process_video_url(values[0], "mp4")
                if values[2]:
                    download.process_video_url(values[0], "mp3")

            if event == sg.WIN_CLOSED or event == 'Beenden':
                print("[Ende] Programm wird beendet.")
                break
            else:
                window.close()
                open_main_frame()
    except Exception:
        traceback.print_exc()
