import os

import pytube
from pytube import YouTube

from config import cfg
from validator import validator


def process_video_url(url, video_type):
    if validator.validate_url(url):

        print("[Download] Prozessiere '" + video_type + "' download für '" + url + "' ...")
        yt = YouTube(url)

        video = yt.streams.first()
        try:

            if video_type == "mp3":
                download_mp3(video)

            if video_type == "mp4":
                download_mp4(video)

        except pytube.exceptions.RegexMatchError:
            print("[Download] Download fehlgeschlagen. ")
    else:
        print("[Download] Der Download wurde abgebrochen, da die URL nicht erkannt wurde.")


def download(video):
    return video.download(output_path=cfg.read_config())


def download_mp4(video):
    download(video)
    download_complete()
    return True


def download_mp3(video):
    downloaded_video = download(video)
    convert_mp4(downloaded_video)
    download_complete()
    return True


def convert_mp4(downloaded_video):
    base, ext = os.path.splitext(downloaded_video)
    new_file = base + '.mp3'
    os.rename(downloaded_video, new_file)


def download_complete():
    print("[Download] Download erfolgreich beendet!")
