import tkinter as tkinter
from config.cfg import create_config, read_config
from gui import gui
from setup import setup
from validator import validator

if __name__ == "__main__":
    create_config()
    print("[Config] Download Pfad: '" + read_config() + "'")

    if len(read_config()) == 0:
        print("[Config] Es existiert kein Pfad.")
        print("[Config] Öffne Setup Popup.")
        setup.open_gui_on_empty_config("Installationspfad angeben")
    gui.open_main_frame()
