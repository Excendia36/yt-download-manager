import os

import PySimpleGUI as sg

from config import cfg


def open_gui_on_empty_config(text):
    file_path = sg.popup_get_folder(text)

    if file_path != "":
        if os.path.exists(file_path):
            cfg.write_config(file_path)
        else:
            print("[Config-Setup] Es wurde ein ungültiger Pfad angegeben.")
            open_gui_on_empty_config("Pfad existiert nicht:")
    else:
        print("[Config-Setup] Es wurde kein Pfad angegeben.")
        open_gui_on_empty_config("Pfad fehlt:")
